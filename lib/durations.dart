final Duration shortPassingPeriod = new Duration(minutes: 5);
final Duration passingPeriod = new Duration(minutes: 8);

final Duration periodOne = new Duration(hours: 1);
final Duration normPeriod = new Duration(minutes: 51);
final Duration wedPeriod = new Duration(minutes: 46);
final Duration minimumPeriod = new Duration(minutes: 34);
final Duration rallyPeriod = new Duration(minutes: 48);
final Duration homecomingPeriod = new Duration(minutes: 48);
final Duration finalPeriod = new Duration(hours: 2, minutes: 8);

final Duration normPeriodFive = new Duration(minutes: 57);
final Duration wedPeriodFive = new Duration(minutes: 50);
final Duration minimumPeriodFive = new Duration(minutes: 38);
final Duration rallyPeriodFive = new Duration(minutes: 52);
final Duration homecomingPeriodThree = new Duration(minutes: 50);
final Duration officeHrs = new Duration(minutes: 20);

final Duration rally = new Duration(minutes: 40);
final Duration homecoming = new Duration(hours: 1);
final Duration lunch = new Duration(minutes: 40);
final Duration minimumLunch = new Duration(minutes: 12);
final Duration finalLunch = new Duration(minutes: 16);

final Duration dayStartNorm = new Duration(hours: 6, minutes: 45);
final Duration dayStartFinal = new Duration(hours: 7, minutes: 50);
final Duration dayStartWed = new Duration(hours: 8, minutes: 50);

final List dayDurationNormal = [
  dayStartNorm,
  shortPassingPeriod,
  periodOne,
  shortPassingPeriod,
  normPeriod,
  passingPeriod,
  normPeriod,
  passingPeriod,
  officeHrs,
  passingPeriod,
  normPeriod,
  passingPeriod,
  normPeriodFive,
  lunch,
  passingPeriod,
  normPeriod,
  passingPeriod,
  normPeriod,
  normPeriod
];

const List dayScheduleNamesNormal = const [
  "Before School",
  "Short Passing Period (→ 1)",
  "Period 1",
  "Short Passing Period (1 → 2)",
  "Period 2",
  "Passing Period (2 → 3)",
  "Period 3",
  "Passing Period (3 → Office Hours)",
  "Office Hours",
  "Passing Period (Office Hours → 4)",
  "Period 4",
  "Passing Period (4 → 5)",
  "Period 5",
  "Lunch",
  "Passing Period (Lunch → 6)",
  "Period 6",
  "Passing Period (6 → 7)",
  "Period 7",
  "School's Out!"
];

final List dayDurationWed = [
  dayStartWed,
  shortPassingPeriod,
  wedPeriod,
  passingPeriod,
  wedPeriod,
  passingPeriod,
  wedPeriod,
  passingPeriod,
  wedPeriodFive,
  lunch,
  passingPeriod,
  wedPeriod,
  passingPeriod,
  wedPeriod,
  wedPeriod
];

const List dayScheduleNamesWed = const [
  "Before School",
  "Short Passing Period (→ 2)",
  "Period 2",
  "Passing Period (2 → 3)",
  "Period 3",
  "Passing Period (3 → 4)",
  "Period 4",
  "Passing Period (4 → 5)",
  "Period 5",
  "Lunch",
  "Passing Period (Lunch → 6)",
  "Period 6",
  "Passing Period (6 → 7)",
  "Period 7",
  "School's Out!"
];

final List dayDurationRally = [
  dayStartNorm,
  shortPassingPeriod,
  periodOne,
  shortPassingPeriod,
  rallyPeriod,
  passingPeriod,
  rallyPeriod,
  passingPeriod,
  rallyPeriod,
  passingPeriod,
  rally,
  passingPeriod,
  rallyPeriodFive,
  lunch,
  passingPeriod,
  rallyPeriod,
  passingPeriod,
  rallyPeriod,
  rallyPeriod
];

const List dayScheduleNamesRally = const [
  "Before School (Rally Day)",
  "Short Passing Period (→ 1)",
  "Period 1",
  "Short Passing Period (1 → 2)",
  "Period 2",
  "Passing Period (2 → 3)",
  "Period 3",
  "Passing Period (3 → 4)",
  "Period 4",
  "Passing Period (4 → Rally)",
  "Rally",
  "Passing Period (Rally → 5)",
  "Period 5",
  "Lunch",
  "Passing Period (Lunch → 6)",
  "Period 6",
  "Passing Period (6 → 7)",
  "Period 7",
  "School's Out!"
];

final List dayDurationMinimum = [
  dayStartNorm,
  shortPassingPeriod,
  periodOne,
  shortPassingPeriod,
  minimumPeriod,
  passingPeriod,
  minimumPeriod,
  passingPeriod,
  minimumPeriod,
  passingPeriod,
  minimumPeriodFive,
  passingPeriod,
  minimumPeriod,
  minimumLunch,
  passingPeriod,
  minimumPeriod,
  minimumPeriod
];

const List dayScheduleNamesMinimum = const [
  "Before School (Minimum Day)",
  "Short Passing Period (→ 1)",
  "Period 1",
  "Short Passing Period (1 → 2)",
  "Period 2",
  "Passing Period (2 → 3)",
  "Period 3",
  "Passing Period (3 → 4)",
  "Period 4",
  "Passing Period (4 → 5)",
  "Period 5",
  "Passing Period (5 → 6)",
  "Period 6",
  "Lunch",
  "Passing Period (Lunch → 7)",
  "Period 7",
  "School's Out!"
];

final List dayDurationFinal = [
  dayStartFinal,
  shortPassingPeriod,
  finalPeriod,
  finalLunch,
  passingPeriod,
  finalPeriod,
  finalPeriod
];

const List dayScheduleNamesFinal = const [
  "Before School (Final Day)",
  "Short Passing Period (→ Final A)",
  "Final A",
  "Break",
  "Passing Period (Break → Final B)",
  "Final B",
  "School's Out!"
];

final List dayDurationHomecoming = [
  dayStartNorm,
  shortPassingPeriod,
  periodOne,
  shortPassingPeriod,
  homecomingPeriod,
  passingPeriod,
  homecomingPeriodThree,
  passingPeriod,
  homecomingPeriod,
  passingPeriod,
  homecoming,
  passingPeriod,
  homecomingPeriod,
  lunch,
  passingPeriod,
  homecomingPeriod,
  passingPeriod,
  homecomingPeriod,
  homecomingPeriod
];

const List dayScheduleNamesHomecoming = const [
  "Before School (Homecoming)",
  "Short Passing Period (→ 1)",
  "Period 1",
  "Short Passing Period (1 → 2)",
  "Period 2",
  "Passing Period (2 → 3)",
  "Period 3",
  "Passing Period (3 → 4)",
  "Period 4",
  "Passing Period (4 → Rally)",
  "Rally",
  "Passing Period (Rally → 5)",
  "Period 5",
  "Lunch",
  "Passing Period (Lunch → 6)",
  "Period 6",
  "Passing Period (6 → 7)",
  "Period 7",
  "School's Out!"
];
