import 'dart:html';
import 'dart:async';
import 'dart:convert';

import 'package:angular2/core.dart';

import 'teacher_timer.dart';

/// Looks for a JSON file at the root of a web server that tells it where to get the rest of the config from.
const _getStringURL = "./settings.json";

/// Injectable class that gets special dates from a remote JSON file.
@Injectable()
class DateGetter {
  /// If the JSON settings have been loaded, [ready] evaluates to true.
  bool ready = false;

  /// Stores the settings retrieved from the remote server.
  var daySettingsJSON;

  /// Stores the settings URL gotten from [_getStringURL].
  String _settingsURL = "";

  /// Gets the URL from [_getStringURL] and then the settings from the JSON residing at _that_ URL.
  Future readyJSON() async {
    if (ready) return; // If we've already done this don't bother doing it again.
    if(_settingsURL.isEmpty) _settingsURL = JSON.decode(await HttpRequest.getString(_getStringURL))['url']; // Get the settings URL.
    var jsonAsString = await HttpRequest.getString(_settingsURL); // Get the JSON settings
    daySettingsJSON = JSON.decode(jsonAsString); // Decode the JSON message
    ready = true; // We're ready to roll!
  }

  /// Returns a stream of rally days from the JSON file.
  Stream<DateTime> getRallyDays() async* {
    await readyJSON(); // Make sure our settings are in
    for (var simpleTime in daySettingsJSON['Rally Days']) {
      yield new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]); // Yield, in a stream, each date.
    }
  }

  /// As [getRallyDays], but minimum days.
  Stream<DateTime> getMinimumDays() async* {
    await readyJSON();
    for (var simpleTime in daySettingsJSON['Minimum Days']) {
      yield new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]);
    }
  }

  /// As [getRallyDays], but final exam days.
  Stream<DateTime> getExamDays() async* {
    await readyJSON();
    for (var simpleTime in daySettingsJSON['Final Exam Days']) {
      yield new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]);
    }
  }

  /// As [getRallyDays], but days off.
  Stream<DateTime> getDaysOff() async* {
    await readyJSON();
    for (var simpleTime in daySettingsJSON['Days Off']) {
      yield new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]);
    }
  }

  /// As [getRallyDays], but abnormal Wednesday schedules.
  Stream<DateTime> getAbnormalWednesdays() async* {
    await readyJSON();
    for (var simpleTime in daySettingsJSON['Abnormal Wednesday Days']) {
      yield new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]);
    }
  }

  /// As [getRallyDays], but abnormal normal schedules.
  Stream<DateTime> getAbnormalNormals() async* {
    await readyJSON();
    for (var simpleTime in daySettingsJSON['Abnormal Normal Days']) {
      yield new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]);
    }
  }

  /// Returns the one homecoming day we have.
  Future<DateTime> getHomecomingDay() async {
    await readyJSON();
    var simpleTime = daySettingsJSON['Homecoming'];
    return new DateTime(simpleTime["year"], simpleTime["month"], simpleTime["day"]);
  }

  /// Queries the JSON settings to see if the [DateTime] given is special in any way.
  Future<DayType> isTodaySpecial(DateTime dt) async {
    DateTime simpleTime = new DateTime(dt.year, dt.month, dt.day); // Strip the time from the given [DateTime]

    if(simpleTime.weekday == DateTime.SATURDAY || simpleTime.weekday == DateTime.SUNDAY)
    {
      return DayType.WEEKEND;
    }

    await for (DateTime possibleDay in getDaysOff()) {
      if (simpleTime.compareTo(possibleDay) == 0) {
        return DayType.DAYOFF;
      }
    }
    await for (DateTime possibleDay in getRallyDays()) {
      if (simpleTime.compareTo(possibleDay) == 0) {
        return DayType.RALLY;
      }
    }
    await for (DateTime possibleDay in getMinimumDays()) {
      if (simpleTime.compareTo(possibleDay) == 0) {
        return DayType.MINIMUM;
      }
    }
    await for (DateTime possibleDay in getExamDays()) {
      if (simpleTime.compareTo(possibleDay) == 0) {
        return DayType.FINAL;
      }
    }
    await for (DateTime possibleDay in getAbnormalWednesdays()) {
      if (simpleTime.compareTo(possibleDay) == 0) {
        return DayType.WEDNESDAY;
      }
    }
    await for (DateTime possibleDay in getAbnormalNormals()) {
      if (simpleTime.compareTo(possibleDay) == 0) {
        return DayType.NORMAL;
      }
    }
    if (simpleTime.compareTo(await getHomecomingDay()) == 0) {
      return DayType.HOMECOMING;
    }



    if(simpleTime.weekday == DateTime.WEDNESDAY)
    {
      return DayType.WEDNESDAY;
    }

    return DayType.NORMAL; // If none of the above are satisfied, it's a normal day.
  }
}
