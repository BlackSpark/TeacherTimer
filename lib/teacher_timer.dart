// Copyright (c) 2016, Jonathan Kula. All rights reserved. Use of this source code

// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';
import 'package:angular2/core.dart';
import 'dart:async';
import 'durations.dart';
import 'date_getter.dart';

/// The default [TeacherTimer.header].
const defaultHeader = "Time Remaining:";

/// The type of school day today is
enum DayType { NORMAL, WEDNESDAY, RALLY, MINIMUM, FINAL, HOMECOMING, WEEKEND, DAYOFF }

/// Defines a timer instance.
///
/// Due to the implementation nature, all timers operate similarly on the same machine.
@Component(
    selector: 'teacher-timer',
    styleUrls: const ['teacher_timer.css'],
    templateUrl: 'teacher_timer.html',
    providers: const [DateGetter])
class TeacherTimer implements OnInit {

  /// Defines the header of the webpage (see [defaultHeader])
  String header = defaultHeader;

  /// Defines the time remaining displayed on the webpage.
  String timeRemaining = "loading...";

  /// Defines the current time displayed on the webpage.
  String currentTime = "loading...";

  /// Defines the current period displayed on the webpage.
  String currentPeriod = "loading...";

  /// Defines the "AM" or "PM" that is attatched to [currentTime].
  String ampm = "";

  /// Defines the current percentage of the progress bar.
  ///
  /// Should be a string representation of an int on the interval 0-100 inclusive.
  String currentProgress = "1";

  /// Defines the current day. Used to reset the application when the day changes.
  int currentDay;

  /// Defines the current day type. Used to select different schedules from [durations.dart].
  DayType currentType;

  /// Defines the [DateGetter] that pulls special dates from a remote JSON file.
  final DateGetter _dateGetter;

  /// Constructs a new TeacherTimer and injects this [_dateGetter].
  TeacherTimer(this._dateGetter);

  /// Defines an initialization function for the [TeacherTimer].
  ///
  /// Creates a new [Timer.periodic] that runs [update] every quarter second, and sets the [currentDay] and [currentType]
  /// using [_dateGetter].
  @override
  ngOnInit() async { // Needs to be async because _dateGetter is wholly asynchronous.
    const quarterSecond = const Duration(milliseconds: 250); // Defines a quarter second at compile time.

    currentDay = new DateTime.now().day; // Sets the current day.
    currentType = await _dateGetter.isTodaySpecial(new DateTime.now()); // Sets the current type by asking if today is special.

    new Timer.periodic(quarterSecond, update); // Runs [update] every quarter second.
    print("Timer Loaded!"); // For debug purposes.
  }

  /// Updates the [TeacherTimer]
  ///
  /// The meat of the program. Computes and formats [header], [timeRemaining], [currentTime],
  /// [currentPeriod], [ampm], and [currentProgress].
  void update(Timer t) {
    DateTime rightNow = new DateTime.now(); // Gets the time right now.
    DateTime dayStart = new DateTime(rightNow.year, rightNow.month, rightNow.day); // Truncates rightNow so it's midnight today.
    Duration fromStart = rightNow.difference(dayStart); // Gets time elapsed time since the start of the day.

    if (rightNow.day != currentDay) {
      window.location.reload(); // If the day has changed, critical variables defined in [ngOnInit] may be changed, so we'll reload the page.
    }

    currentTime = // Sets the current time in the format H:MM:SS (12h), making sure to keep the leading zero in MM and SS
    "${((rightNow.hour - 1) % 12) + 1}:${rightNow.minute.toString().padLeft(
        2, "0")}:${rightNow.second.toString().padLeft(2, "0")}";
    ampm = " ${rightNow.hour < 12 ? "AM" : "PM"}"; // Sets the current AM or PM

    if(currentType == DayType.WEEKEND || currentType == DayType.DAYOFF)
    { // If it's the weekend or a day off, we don't need to compute any timers. There's no school today.
      timeRemaining = "-- : --";
      currentPeriod = "It's ${currentType == DayType.WEEKEND ? "the weekend" : "a day off"}!";
      currentProgress = "101"; // Solid blue progress bar
      header = "CHS TeacherTimer by Jonathan Kula";
      return; // Stop evaluation
    }

    int maxCheckLength = getDurationListLength(currentType) + 1; // Gets the max length for the for loop.

    // This for loop iterates by number of bells (including passing bells, etc)
    for (int bellnum = 1; bellnum <= maxCheckLength; bellnum++) {
      Duration currentPeriodFromStart = getRelativeDuration(bellnum, currentType); // Gets the elapsed time from midnight to the current bell number.
      int afterOrBefore = fromStart.compareTo(currentPeriodFromStart); // -1 if we're before this bell, 0 if we exactly equal it, and 1 if we're after.
      if (afterOrBefore < 0) { // If we're after or at the bell, we've already passed this period!
        Duration timeleft = currentPeriodFromStart - fromStart; // Time left in the period
        List<int> brokenTime = getHMSMs(timeleft); // Breaks the duration into hours/minutes/seconds
        // Formats the time remaining in the format (H):MM:SS
        timeRemaining = "${brokenTime[0] == 0 ? "" : "${brokenTime[0]} : "}${brokenTime[1].toString().padLeft(2, "0")} : ${brokenTime[2].toString().padLeft(2, "0")}";
        Duration currentPeriodDuration = getDurationListValue(bellnum - 1, currentType);  // Gets the length of this period.
        // Sets the progress bar to the rounded percentage of the period's duration and the time left.
        // Percent becomes higher as the period is closer to completion.
        currentProgress = (((currentPeriodDuration - timeleft).inSeconds /
            currentPeriodDuration.inSeconds) * 100).round().toString();
        currentPeriod = getDurationNameValue(bellnum - 1, currentType); // Gets the name of the period
        if(header != defaultHeader) header = defaultHeader; // Sets the header to the default header if it's not already that.
        break; // End the for loop
      }
      if (bellnum == maxCheckLength) { // This means that school's out.
        currentProgress = "101"; // Solid Aqua Progress Bar (means the timer's over)
        Duration timeElapsed = fromStart -
            getRelativeDuration(maxCheckLength - 1, currentType); // Time elapsed since the beginning of school
        List<int> brokenTime = getHMSMs(timeElapsed); // Splits up the duration into hours/minutes/seconds
        timeRemaining = // Sets the time elapsed in the format +(H) :  MM : SS
        "+${brokenTime[0] == 0 ? "" : "${brokenTime[0]} : "}${brokenTime[1]
            .toString()
            .padLeft(2, "0")} : ${brokenTime[2].toString().padLeft(2, "0")}";
        currentPeriod = getDurationNameValue(bellnum - 2, currentType);
        header = "Time Elapsed:"; // Sets the header to the time elapsed since we're counting up now.
        break; // End the for loop
      }
    }
  }

  /// Takes a [Duration] and splits it into a list of the form (Hours, Minutes % 60, Seconds % 60, MS % 1000).
  static List<int> getHMSMs(Duration d) {
    int ms = d.inMilliseconds; // Gets the duration in ms to do all the transformations with.
    int s = ((ms / 1000) % 60).floor();
    int m = (((ms / 1000) / 60) % 60).floor();
    int h = (((ms / 1000) / 60) / 60).floor();
    return [h, m, s, ms % 1000];
  }

  /// Gets the proper duration list from [durations.dart] given a [DayType] and gets the length from that.
  static int getDurationListLength(DayType t)
  {
    switch (t) {
      case DayType.NORMAL:
        return dayDurationNormal.length;
      case DayType.WEDNESDAY:
        return dayDurationWed.length;
      case DayType.RALLY:
        return dayDurationRally.length;
      case DayType.MINIMUM:
        return dayDurationMinimum.length;
      case DayType.FINAL:
        return dayDurationFinal.length;
      case DayType.HOMECOMING:
        return dayDurationHomecoming.length;
      default:
        return -1;
    }
  }

  /// Gets the proper duration list from [durations.dart] given a [DayType] and gets the value of [index] from that.
  static Duration getDurationListValue(int index, DayType t)
  {
    switch (t) {
      case DayType.NORMAL:
        return dayDurationNormal[index];
      case DayType.WEDNESDAY:
        return dayDurationWed[index];
      case DayType.RALLY:
        return dayDurationRally[index];
      case DayType.MINIMUM:
        return dayDurationMinimum[index];
      case DayType.FINAL:
        return dayDurationFinal[index];
      case DayType.HOMECOMING:
        return dayDurationHomecoming[index];
      default:
        return new Duration();
    }
  }

  /// Gets the proper duration list from [durations.dart] given a [DayType] and gets the period name at [index] from that.
  static String getDurationNameValue(int index, DayType t)
  {
    switch (t) {
      case DayType.NORMAL:
        return dayScheduleNamesNormal[index];
      case DayType.WEDNESDAY:
        return dayScheduleNamesWed[index];
      case DayType.RALLY:
        return dayScheduleNamesRally[index];
      case DayType.MINIMUM:
        return dayScheduleNamesMinimum[index];
      case DayType.FINAL:
        return dayScheduleNamesFinal[index];
      case DayType.HOMECOMING:
        return dayScheduleNamesHomecoming[index];
      default:
        return "";
    }
  }

  /// Gets the duration to some [bellNumber] from the beginning of the day, given a [DayType] [t].
  static Duration getRelativeDuration(int bellNumber, DayType t) {
    assert(bellNumber > 0);
    Duration retVal = new Duration();
    int maxI = getDurationListLength(t) - 1;
    for (int i = 0; i < bellNumber && i < maxI; i++) {
      retVal += getDurationListValue(i, t); // Adds up all the bell durations to get a the cumulative one from the beginning of the day.
    }
    return retVal;
  }
}
